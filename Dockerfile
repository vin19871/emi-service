FROM adoptopenjdk/openjdk11:alpine-jre
ARG JAR_FILE=target/*.jar
WORKDIR /opt/app
COPY ${JAR_FILE} emi-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","emi-0.0.1-SNAPSHOT.jar"]