package io.vintest.emi;

import java.util.Optional;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmiCalculatorService {

    @GetMapping(path="/emi/{p}/{r}/{n}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Optional<EmiServiceResponse> calculateEmi(@PathVariable("p") String principalAmount ,
    @PathVariable("r") String iRate,
    @PathVariable("n") String numberOfyears){
        
    

        Optional<EmiServiceResponse> respone = Optional.empty(); ; 
        
        double principal= Double.parseDouble(principalAmount);  
        double rate= Double.parseDouble(iRate)/(12*100);  
        double time= Double.parseDouble(numberOfyears)*12;  
        
       
        double emi= (principal*rate*Math.pow(1+rate,time))/(Math.pow(1+rate,time)-1);
        double totalAmount = emi*time; 
        double totalInterestAmount = totalAmount-principal; 
        double monthlyIntrestOnly = totalInterestAmount / time;
        double monthlyTowardsPrincipal = emi- monthlyIntrestOnly;
        
        respone = Optional.of(new EmiServiceResponse((int)emi,(int)totalAmount,(int)totalInterestAmount,(int)monthlyIntrestOnly,(int)monthlyTowardsPrincipal) );
        
        return respone;

    }
    
}
