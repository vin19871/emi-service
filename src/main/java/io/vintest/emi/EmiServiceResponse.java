package io.vintest.emi;

import java.io.Serializable;

import lombok.NoArgsConstructor;


@NoArgsConstructor
public class EmiServiceResponse implements Serializable {
    private static final long serialVersionUID = 1L;
   
    private int emi;
    private int totalAmount;
    private int totalIntreset;
    private int monthlyIntrestOnly;
    private int monthlyTowardsPrincipal;

    public EmiServiceResponse(int emi, int totalAmount, int totalIntreset, int monthlyIntrestOnly,
            int monthlyTowardsPrincipal) {
        this.emi = emi;
        this.totalAmount = totalAmount;
        this.totalIntreset = totalIntreset;
        this.monthlyIntrestOnly = monthlyIntrestOnly;
        this.monthlyTowardsPrincipal = monthlyTowardsPrincipal;
    }

    public int getMonthlyIntrestOnly() {
        return monthlyIntrestOnly;
    }

    public void setMonthlyIntrestOnly(int monthlyIntrestOnly) {
        this.monthlyIntrestOnly = monthlyIntrestOnly;
    }

    public int getMonthlyTowardsPrincipal() {
        return monthlyTowardsPrincipal;
    }

    public void setMonthlyTowardsPrincipal(int monthlyTowardsPrincipal) {
        this.monthlyTowardsPrincipal = monthlyTowardsPrincipal;
    }

    public EmiServiceResponse(int emi, int totalAmount, int totalIntreset) {
        this.emi = emi;
        this.totalAmount = totalAmount;
        this.totalIntreset = totalIntreset;
    }

    public int getTotalIntreset() {
        return totalIntreset;
    }

    public void setTotalIntreset(int totalIntreset) {
        this.totalIntreset = totalIntreset;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getEmi() {
        return emi;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    


    public void setEmi(int emi) {
        this.emi = emi;
    }
 
    
}
